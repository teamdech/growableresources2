package teamdech.growableresources.proxies;

import cpw.mods.fml.client.registry.RenderingRegistry;
import teamdech.growableresources.client.MiniatureBlockRenderer;
import teamdech.growableresources.client.ReedRenderer;

public class ClientProxy extends ServerProxy {
    public static int reedRendererID = 0, miniBlockRendererID;
    //public static ReedRenderer rr;

    @Override
    public void init() {
        super.init();
        reedRendererID = RenderingRegistry.getNextAvailableRenderId();
        RenderingRegistry.registerBlockHandler(new ReedRenderer());
        miniBlockRendererID = RenderingRegistry.getNextAvailableRenderId();
        RenderingRegistry.registerBlockHandler(new MiniatureBlockRenderer());
    }
}

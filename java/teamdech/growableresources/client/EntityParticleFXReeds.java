package teamdech.growableresources.client;

import net.minecraft.client.particle.EntityFX;
import net.minecraft.world.World;
import teamdech.growableresources.blocks.BlockResourceReeds;
import teamdech.growableresources.resources.Resource;

import java.util.Random;

public class EntityParticleFXReeds extends EntityFX {

    boolean special = false;
    Random random = new Random();
    int ticks = 0;

    @Deprecated
    public EntityParticleFXReeds(World world, double x, double y, double z, Resource resource) {
        super(world, x, y, z);
        setParticleIcon(BlockResourceReeds.iconParticle);

        particleScale = 0.2F;

        particleMaxAge = 80;

        particleRed = resource.r;
        particleGreen = resource.g;
        particleBlue = resource.b;

        motionX = 0;
        motionY = 0.01F;
        motionZ = 0;
    }

    public EntityParticleFXReeds(World world, double x, double y, double z, Resource resource, boolean special) {
        super(world, x, y, z);

        this.special = special;

        setParticleIcon(BlockResourceReeds.iconParticle);

        particleScale = 0.2F;

        particleMaxAge = 80;

        particleRed = resource.r;
        particleGreen = resource.g;
        particleBlue = resource.b;

        motionX = 0;
        motionY = 0.01F;
        motionZ = 0;
    }

    @Override
    public void onUpdate() {
        super.onUpdate();
        ticks++;

        if (special && (ticks % 10 == 0)) {
            particleRed = random.nextFloat();
            particleGreen = random.nextFloat();
            particleBlue = random.nextFloat();
        }

        particleAlpha = 1F - ((float) particleAge) / ((float) particleMaxAge);
    }

    @Override
    public int getFXLayer() {
        return 1;
    }
}

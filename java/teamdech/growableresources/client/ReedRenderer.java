package teamdech.growableresources.client;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import org.lwjgl.opengl.GL11;
import teamdech.growableresources.blocks.BlockResourceReeds;
import teamdech.growableresources.blocks.Blocks;
import teamdech.growableresources.blocks.TileEntityResourceReeds;
import teamdech.growableresources.proxies.ClientProxy;
import teamdech.growableresources.resources.Resource;
import teamdech.growableresources.resources.Resources;

public class ReedRenderer implements ISimpleBlockRenderingHandler {
    @Override
    public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block reeds, int modelId, RenderBlocks renderer) {
        Tessellator t = Tessellator.instance;

        GL11.glPushMatrix();

        t.setBrightness(Blocks.blockReeds.getMixedBrightnessForBlock(world, x, y, z));
        t.setColorOpaque_F(1, 1, 1);

        IIcon base = BlockResourceReeds.iconBase;
        IIcon overlay = BlockResourceReeds.iconOverlay;

        float minU = base.getMinU();
        float maxU = base.getMaxU();
        float minV = base.getMinV();
        float maxV = base.getMaxV();

        float x1 = x + 0.05F, x2 = x + 0.95F;
        float z1 = z + 0.05F, z2 = z + 0.95F;

        t.addVertexWithUV(x1, y + 1, z1, minU, minV);
        t.addVertexWithUV(x1, y, z1, minU, maxV);
        t.addVertexWithUV(x2, y, z2, maxU, maxV);
        t.addVertexWithUV(x2, y + 1, z2, maxU, minV);
        t.addVertexWithUV(x2, y + 1, z2, minU, minV);
        t.addVertexWithUV(x2, y, z2, minU, maxV);
        t.addVertexWithUV(x1, y, z1, maxU, maxV);
        t.addVertexWithUV(x1, y + 1, z1, maxU, minV);
        t.addVertexWithUV(x1, y + 1, z2, minU, minV);
        t.addVertexWithUV(x1, y, z2, minU, maxV);
        t.addVertexWithUV(x2, y, z1, maxU, maxV);
        t.addVertexWithUV(x2, y + 1, z1, maxU, minV);
        t.addVertexWithUV(x2, y + 1, z1, minU, minV);
        t.addVertexWithUV(x2, y, z1, minU, maxV);
        t.addVertexWithUV(x1, y, z2, maxU, maxV);
        t.addVertexWithUV(x1, y + 1, z2, maxU, minV);

        minU = overlay.getMinU();
        maxU = overlay.getMaxU();
        minV = overlay.getMinV();
        maxV = overlay.getMaxV();

        Resource r = Resources.getResourceByID(((TileEntityResourceReeds) Minecraft.getMinecraft().theWorld.getTileEntity(x, y, z)).resourceID);

        t.setColorOpaque_F(r.r, r.g, r.b);

        t.addVertexWithUV(x1, y + 1, z1, minU, minV);
        t.addVertexWithUV(x1, y, z1, minU, maxV);
        t.addVertexWithUV(x2, y, z2, maxU, maxV);
        t.addVertexWithUV(x2, y + 1, z2, maxU, minV);
        t.addVertexWithUV(x2, y + 1, z2, minU, minV);
        t.addVertexWithUV(x2, y, z2, minU, maxV);
        t.addVertexWithUV(x1, y, z1, maxU, maxV);
        t.addVertexWithUV(x1, y + 1, z1, maxU, minV);
        t.addVertexWithUV(x1, y + 1, z2, minU, minV);
        t.addVertexWithUV(x1, y, z2, minU, maxV);
        t.addVertexWithUV(x2, y, z1, maxU, maxV);
        t.addVertexWithUV(x2, y + 1, z1, maxU, minV);
        t.addVertexWithUV(x2, y + 1, z1, minU, minV);
        t.addVertexWithUV(x2, y, z1, minU, maxV);
        t.addVertexWithUV(x1, y, z2, maxU, maxV);
        t.addVertexWithUV(x1, y + 1, z2, maxU, minV);

        GL11.glPopMatrix();
        t.setColorOpaque_F(1, 1, 1);

        return true;
    }

    @Override
    public void renderInventoryBlock(Block block, int metadata, int modelID, RenderBlocks renderer) {
    }

    @Override
    public int getRenderId() {
        return ClientProxy.reedRendererID;
    }

    public static void renderPreview(IBlockAccess world, float x, int y, float z, int reedType) {
        x -= 0.5F;
        z -= 0.5F;

        Tessellator t = Tessellator.instance;

        Minecraft.getMinecraft().renderEngine.bindTexture(TextureMap.locationBlocksTexture);

        t.startDrawingQuads();
        t.setColorOpaque_F(1, 1, 1);

        IIcon soil = Blocks.blockFertilizedSoil.getBlockTextureFromSide(0);
        IIcon base = BlockResourceReeds.iconBase;
        IIcon overlay = BlockResourceReeds.iconOverlay;

        float minU = soil.getMinU();
        float maxU = soil.getMaxU();
        float minV = soil.getMinV();
        float maxV = soil.getMaxV();

        float x1 = x + 0.05F, x2 = x + 0.95F;
        float z1 = z + 0.05F, z2 = z + 0.95F;

        t.addVertexWithUV(x, y, z, minU, minV);
        t.addVertexWithUV(x, y, z + 1, minU, maxV);
        t.addVertexWithUV(x + 1, y, z + 1, maxU, maxV);
        t.addVertexWithUV(x + 1, y, z, maxU, minV);

        minU = base.getMinU();
        maxU = base.getMaxU();
        minV = base.getMinV();
        maxV = base.getMaxV();

        t.addVertexWithUV(x1, y + 1, z1, minU, minV);
        t.addVertexWithUV(x1, y, z1, minU, maxV);
        t.addVertexWithUV(x2, y, z2, maxU, maxV);
        t.addVertexWithUV(x2, y + 1, z2, maxU, minV);
        t.addVertexWithUV(x2, y + 1, z2, minU, minV);
        t.addVertexWithUV(x2, y, z2, minU, maxV);
        t.addVertexWithUV(x1, y, z1, maxU, maxV);
        t.addVertexWithUV(x1, y + 1, z1, maxU, minV);
        t.addVertexWithUV(x1, y + 1, z2, minU, minV);
        t.addVertexWithUV(x1, y, z2, minU, maxV);
        t.addVertexWithUV(x2, y, z1, maxU, maxV);
        t.addVertexWithUV(x2, y + 1, z1, maxU, minV);
        t.addVertexWithUV(x2, y + 1, z1, minU, minV);
        t.addVertexWithUV(x2, y, z1, minU, maxV);
        t.addVertexWithUV(x1, y, z2, maxU, maxV);
        t.addVertexWithUV(x1, y + 1, z2, maxU, minV);

        minU = overlay.getMinU();
        maxU = overlay.getMaxU();
        minV = overlay.getMinV();
        maxV = overlay.getMaxV();

        Resource r = Resources.getResourceByID(reedType);

        t.setColorOpaque_F(r.r, r.g, r.b);

        t.addVertexWithUV(x1, y + 1, z1, minU, minV);
        t.addVertexWithUV(x1, y, z1, minU, maxV);
        t.addVertexWithUV(x2, y, z2, maxU, maxV);
        t.addVertexWithUV(x2, y + 1, z2, maxU, minV);
        t.addVertexWithUV(x2, y + 1, z2, minU, minV);
        t.addVertexWithUV(x2, y, z2, minU, maxV);
        t.addVertexWithUV(x1, y, z1, maxU, maxV);
        t.addVertexWithUV(x1, y + 1, z1, maxU, minV);
        t.addVertexWithUV(x1, y + 1, z2, minU, minV);
        t.addVertexWithUV(x1, y, z2, minU, maxV);
        t.addVertexWithUV(x2, y, z1, maxU, maxV);
        t.addVertexWithUV(x2, y + 1, z1, maxU, minV);
        t.addVertexWithUV(x2, y + 1, z1, minU, minV);
        t.addVertexWithUV(x2, y, z1, minU, maxV);
        t.addVertexWithUV(x1, y, z2, maxU, maxV);
        t.addVertexWithUV(x1, y + 1, z2, maxU, minV);

        t.setColorOpaque_F(1, 1, 1);

        t.draw();
    }

    @Override
    public boolean shouldRender3DInInventory(int modelId) {
        return false;
    }
}

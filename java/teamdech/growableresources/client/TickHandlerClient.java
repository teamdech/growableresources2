package teamdech.growableresources.client;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;

public class TickHandlerClient {

    public static short rotation;

    @SubscribeEvent
    public static void onTick(TickEvent event) {
        rotation = (short) (rotation % 359 + 1);
    }
}

package teamdech.growableresources.client;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import teamdech.growableresources.proxies.ClientProxy;

public class MiniatureBlockRenderer implements ISimpleBlockRenderingHandler {

    float minU, maxU, minV, maxV;

    @Override
    public void renderInventoryBlock(Block block, int metadata, int modelID, RenderBlocks renderer) {
    }

    @Override
    public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer) {
        /*Tessellator t = Tessellator.instance;

        TileEntityMiniatureBlock tile = (TileEntityMiniatureBlock) world.getTileEntity(x, y, z);
        if (tile == null) return true;
        if (tile.blockName.equals("stone") || tile.) tile.blockID = 1;

        t.setBrightness(Blocks.blockMiniBlock.getMixedBrightnessForBlock(world, x, y, z));
        t.setColorOpaque_F(1F, 1F, 1F);

        bindTexture(Block.blocksList[tile.blockID].getIcon((Block.blocksList[tile.blockID].getRenderType() == 16 ? 1 : 0), tile.damage), t);

        t.addVertexWithUV(x + 0.5F - 0.2F, y, z + 0.5F - 0.2F, minU, minV);
        t.addVertexWithUV(x + 0.5F + 0.2F, y, z + 0.5F - 0.2F, maxU, minV);
        t.addVertexWithUV(x + 0.5F + 0.2F, y, z + 0.5F + 0.2F, maxU, maxV);
        t.addVertexWithUV(x + 0.5F - 0.2F, y, z + 0.5F + 0.2F, minU, maxV);

        bindTexture(Block.blocksList[tile.blockID].getIcon((Block.blocksList[tile.blockID].getRenderType() == 16 ? 0 : 1), tile.damage), t);

        t.addVertexWithUV(x + 0.5F - 0.2F, y + 0.4F, z + 0.5F - 0.2F, minU, minV);
        t.addVertexWithUV(x + 0.5F - 0.2F, y + 0.4F, z + 0.5F + 0.2F, minU, maxV);
        t.addVertexWithUV(x + 0.5F + 0.2F, y + 0.4F, z + 0.5F + 0.2F, maxU, maxV);
        t.addVertexWithUV(x + 0.5F + 0.2F, y + 0.4F, z + 0.5F - 0.2F, maxU, minV);

        bindTexture(Block.blocksList[tile.blockID].getIcon(2, tile.damage), t);

        t.addVertexWithUV(x + 0.5F + 0.2F, y + 0.4F, z + 0.5F - 0.2F, maxU, minV);
        t.addVertexWithUV(x + 0.5F + 0.2F, y + 0.4F, z + 0.5F + 0.2F, minU, minV);
        t.addVertexWithUV(x + 0.5F + 0.2F, y, z + 0.5F + 0.2F, minU, maxV);
        t.addVertexWithUV(x + 0.5F + 0.2F, y, z + 0.5F - 0.2F, maxU, maxV);

        bindTexture(Block.blocksList[tile.blockID].getIcon(3, tile.damage), t);

        t.addVertexWithUV(x + 0.5F + 0.2F, y + 0.4F, z + 0.5F + 0.2F, maxU, minV);
        t.addVertexWithUV(x + 0.5F - 0.2F, y + 0.4F, z + 0.5F + 0.2F, minU, minV);
        t.addVertexWithUV(x + 0.5F - 0.2F, y, z + 0.5F + 0.2F, minU, maxV);
        t.addVertexWithUV(x + 0.5F + 0.2F, y, z + 0.5F + 0.2F, maxU, maxV);

        bindTexture(Block.blocksList[tile.blockID].getIcon(4, tile.damage), t);

        t.addVertexWithUV(x + 0.5F - 0.2F, y + 0.4F, z + 0.5F + 0.2F, maxU, minV);
        t.addVertexWithUV(x + 0.5F - 0.2F, y + 0.4F, z + 0.5F - 0.2F, minU, minV);
        t.addVertexWithUV(x + 0.5F - 0.2F, y, z + 0.5F - 0.2F, minU, maxV);
        t.addVertexWithUV(x + 0.5F - 0.2F, y, z + 0.5F + 0.2F, maxU, maxV);

        bindTexture(Block.blocksList[tile.blockID].getIcon(5, tile.damage), t);

        t.addVertexWithUV(x + 0.5F - 0.2F, y + 0.4F, z + 0.5F - 0.2F, maxU, minV);
        t.addVertexWithUV(x + 0.5F + 0.2F, y + 0.4F, z + 0.5F - 0.2F, minU, minV);
        t.addVertexWithUV(x + 0.5F + 0.2F, y, z + 0.5F - 0.2F, minU, maxV);
        t.addVertexWithUV(x + 0.5F - 0.2F, y, z + 0.5F - 0.2F, maxU, maxV);
         */
        return true;
    }

    @Override
    public boolean shouldRender3DInInventory(int i) {
        return false;
    }

    public void bindTexture(IIcon icon, Tessellator t) {
        t.setColorOpaque_F(1F, 1F, 1F);
        minU = icon.getMinU();
        maxU = icon.getMaxU();
        minV = icon.getMinV();
        maxV = icon.getMaxV();
        if (icon.getIconName().equals("leaves_oak"))
            t.setColorOpaque_I(net.minecraft.init.Blocks.leaves.getRenderColor(0));
        if (icon.getIconName().equals("leaves_spruce"))
            t.setColorOpaque_I(net.minecraft.init.Blocks.leaves.getRenderColor(1));
        if (icon.getIconName().equals("leaves_birch"))
            t.setColorOpaque_I(net.minecraft.init.Blocks.leaves.getRenderColor(2));
        if (icon.getIconName().equals("grass_top"))
            t.setColorOpaque_I(net.minecraft.init.Blocks.grass.getRenderColor(0));

    }

    @Override
    public int getRenderId() {
        return ClientProxy.miniBlockRendererID;
    }
}

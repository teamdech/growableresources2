package teamdech.growableresources.resources;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;
import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.List;

public class Resources {

    public static List<Resource> resourceList = new ArrayList<Resource>();
    private static List<String> idList = new ArrayList<String>();
    private static List<String> nameList = new ArrayList<String>();

    public Resources() {
        add("Sugar", 0, EnumRarity.common, 0.9F, 1F, 0.9F, new ItemStack(Items.sugar), 40, -1);
        add("Iron", 1, EnumRarity.uncommon, 0.737F, 0.6F, 0.502F, new ItemStack(Items.iron_ingot), 60, 34);
        add("Gold", 2, EnumRarity.rare, 0.973F, 0.686F, 0.169F, new ItemStack(Items.gold_ingot), 80, 1);
        add("Diamond", 3, EnumRarity.epic, 0.364F, 0.925F, 0.960F, new ItemStack(Items.diamond), 100, 2);
        add("Emerald", 4, EnumRarity.epic, 0.0F, 0.482F, 0.094F, new ItemStack(Items.emerald), 90, 3);
        add("Lapis", 5, EnumRarity.uncommon, 0.082F, 0.172F, 0.639F, new ItemStack(Items.dye, 1, 4), 65, 29);
        add("Glowstone", 6, EnumRarity.uncommon, 1.0F, 0.737F, 0.368F, new ItemStack(Items.glowstone_dust), 55, 29);
        add("Clay", 7, EnumRarity.common, 0.529F, 0.556F, 0.647F, new ItemStack(Items.clay_ball), 30, 0);
        add("Nether Quartz", 8, EnumRarity.uncommon, 1F, 1F, 1F, new ItemStack(Items.quartz), 60, 5);
        add("Brown Mushroom", 9, EnumRarity.common, 0.568F, 0.427F, 0.333F, new ItemStack(Blocks.brown_mushroom_block), 35, 0);
        add("Red Mushroom", 10, EnumRarity.common, 0.886F, 0.070F, 0.070F, new ItemStack(Blocks.red_mushroom_block), 35, 0);
        add("Ender Pearl", 11, EnumRarity.epic, 0.043F, 0.301F, 0.258F, new ItemStack(Items.ender_pearl), 70, 24);
        add("Blaze Rod", 12, EnumRarity.rare, 0.682F, 0.235F, 0F, new ItemStack(Items.blaze_rod), 70, 6);
        add("Wither Skeleton Skull", 13, EnumRarity.epic, 0.278F, 0.302F, 0.302F, new ItemStack(Items.skull, 1, 1), 120, 3);
        add("Cookie", 14, EnumRarity.common, 0.851F, 0.514F, 0.243F, new ItemStack(Items.cookie), 50, 18);
        add("Slimeball", 15, EnumRarity.rare, 0.49F, 0.784F, 0.451F, new ItemStack(Items.slime_ball), 60, 29);
        add("Potato", 16, EnumRarity.uncommon, 0.729F, 0.475F, 0.082F, new ItemStack(Items.potato), 45, 18);
        add("Carrot", 17, EnumRarity.uncommon, 1.0F, 0.569F, 0.106F, new ItemStack(Items.carrot), 45, 18);
        add("Wheat", 18, EnumRarity.common, 0.318F, 0.369F, 0.067F, new ItemStack(Items.wheat), 40, 0);
        add("Ghast Tear", 19, EnumRarity.uncommon, 0.663F, 0.745F, 0.745F, new ItemStack(Items.ghast_tear), 60, 25);
        add("Leather", 20, EnumRarity.common, 0.776F, 0.361F, 0.208F, new ItemStack(Items.leather), 50, 31);
        add("Experience Bottle", 21, EnumRarity.epic, 0.71F, 0.894F, 0.353F, new ItemStack(Items.experience_bottle), 80, 20);
        add("Egg", 22, EnumRarity.common, 0.702F, 0.647F, 0.49F, new ItemStack(Items.egg), 45, 32);
        add("Apple", 23, EnumRarity.common, 1.0F, 0.369F, 0.412F, new ItemStack(Items.apple), 45, 18);
        add("Obsidian", 24, EnumRarity.uncommon, 0.235F, 0.188F, 0.337F, new ItemStack(Blocks.obsidian), 70, 25);
        add("Soul Sand", 25, EnumRarity.common, 0.506F, 0.416F, 0.357F, new ItemStack(Blocks.soul_sand), 45, 0);
        add("Wool", 26, EnumRarity.common, 0.565F, 0.565F, 0.565F, new ItemStack(Blocks.wool), 50, 28);
        add("Ink Sac", 27, EnumRarity.common, 0.443F, 0.404F, 0.451F, new ItemStack(Items.dye), 40, 0);
        add("String", 28, EnumRarity.common, 1F, 1F, 1F, new ItemStack(Items.string), 35, 0);
        add("Redstone", 29, EnumRarity.uncommon, 0.286F, 0.0F, 0.0F, new ItemStack(Items.redstone), 60, 34);
        add("Raw Porkchop", 30, EnumRarity.common, 0.522F, 0.243F, 0.243F, new ItemStack(Items.porkchop), 45, 17);
        add("Raw Beef", 31, EnumRarity.common, 0.678F, 0.2F, 0.18F, new ItemStack(Items.beef), 45, 30);
        add("Raw Chicken", 32, EnumRarity.common, 0.937F, 0.737F, 0.675F, new ItemStack(Items.chicken), 40, 30);
        add("Gunpowder", 33, EnumRarity.uncommon, 0.212F, 0.212F, 0.212F, new ItemStack(Items.gunpowder), 60, 29);
        add("Coal", 34, EnumRarity.common, 0.152F, 0.094F, 0.0F, new ItemStack(Items.coal), 55, 0);
        add("Copper", 35, EnumRarity.rare, 0.832F, 0.457F, 0.027F, null, 50, 1);
        add("Tin", 36, EnumRarity.rare, 0.875F, 0.917F, 0.910F, null, 55, 1);
        add("White Glow Crystal", 37, EnumRarity.rare, 1F, 1F, 1F, null, 40, 0);
        add("Black Glow Crystal", 38, EnumRarity.rare, 0F, 0F, 0F, null, 40, 0);
        add("Red Glow Crystal", 39, EnumRarity.rare, 0.545F, 0.0F, 0.0F, null, 40, 0);
        add("Orange Glow Crystal", 40, EnumRarity.rare, 1.0F, 0.549F, 0.0F, null, 40, 0);
        add("Yellow Glow Crystal", 41, EnumRarity.rare, 1.0F, 1.0F, 0.0F, null, 40, 0);
        add("Green Glow Crystal", 42, EnumRarity.rare, 0.196F, 0.804F, 0.196F, null, 40, 0);
        add("Light Blue Glow Crystal", 43, EnumRarity.rare, 0.529F, 0.808F, 0.922F, null, 40, 0);
        add("Blue Glow Crystal", 44, EnumRarity.rare, 0.0F, 0.0F, 0.545F, null, 40, 0);
        add("Purple Glow Crystal", 45, EnumRarity.rare, 0.502F, 0.0F, 0.502F, null, 40, 0);
        add("Pink Glow Crystal", 46, EnumRarity.rare, 1.0F, 0.078F, 0.576F, null, 40, 0);
        add("Special Glow Crystal", 47, EnumRarity.rare, 0, 0, 0, null, 40, 0);
        add("Shadow Ingot", 48, EnumRarity.epic, 0.18F, 0.18F, 0.18F, null, 60, 2);
    }

    public static void add(String name, int id, EnumRarity rarity, float r, float g, float b, ItemStack product, int base, int origin) {
        resourceList.add(new Resource(name, id, rarity, r, g, b, product, base, origin));
        idList.add(id + "");
        nameList.add(name.split(" ")[0]);
    }

    public static int getNextAvailableID() {
        return resourceList.size();
    }

    public static Resource getResourceByID(int resourceID) {
        try {
            resourceList.get(resourceID);
        } catch (IndexOutOfBoundsException e) {
            throw new IllegalStateException("Resource number " + resourceID + " does not exist!");
        }
        return resourceList.get(resourceID);
    }

    public static Resource getResourceByName(String name) {

        for (Resource resource : resourceList) {
            if (resource.name.toLowerCase().contains(name.toLowerCase())) return resource;
        }

        throw new IllegalStateException("Resource '" + name + "' does not exist!");
    }

    public static String[] getIDList() {
        return idList.toArray(new String[]{});
    }

    public static String[] getNameList() {
        return nameList.toArray(new String[]{});
    }

    public static String[] getCompleteList() {
        return ArrayUtils.addAll(getNameList(), getIDList());
    }
}
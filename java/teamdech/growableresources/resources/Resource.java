package teamdech.growableresources.resources;

import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;

public class Resource {

    public String name;
    public int id, infusionBaseValue;
    public float r, g, b;
    public ItemStack product;
    public EnumRarity rarity;
    public int origin;
    public boolean active;

    public Resource(String name, int id, EnumRarity rarity, float r, float g, float b, ItemStack product, int infusionBaseValue, int origin) {
        this.name = name;
        this.id = id;
        this.r = r;
        this.g = g;
        this.b = b;
        this.rarity = rarity;
        this.product = product;
        this.infusionBaseValue = infusionBaseValue;
        this.origin = origin;
        active = true;
    }

    @Override
    public String toString() {
        return "Resource " + id + ": " + name;
    }

    public void disable() {
        active = false;
    }
}

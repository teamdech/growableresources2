package teamdech.growableresources.blocks;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class TileEntityMiniatureBlock extends TileEntity {

    public int damage;
    public String blockName;

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        blockName = compound.getString("blockName");
        damage = compound.getInteger("damage");
    }

    @Override
    public void writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        compound.setString("blockName", blockName);
        compound.setInteger("damage", damage);
    }
    /* TODO PACKET HANDLING */
//    @Override
//    public Packet getDescriptionPacket() {
//        NBTTagCompound data = new NBTTagCompound();
//        writeToNBT(data);
//        return new Packet132TileEntityData(xCoord, yCoord, zCoord, 1, data);
//    }
//
//    @Override
//    public void onDataPacket(INetworkManager net, Packet132TileEntityData packet) {
//        readFromNBT(packet.data);
//        worldObj.markBlockForRenderUpdate(xCoord, yCoord, zCoord);
//    }
}

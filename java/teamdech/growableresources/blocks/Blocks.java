package teamdech.growableresources.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import teamdech.growableresources.GrowableResources;

public class Blocks {
    public static Block blockReeds, blockFertilizedSoil, blockShredder, blockInfuser, blockMiniBlock;

    public static void registerBlocks() {
        blockReeds = new BlockResourceReeds().setBlockName("blockReeds");
        GameRegistry.registerBlock(blockReeds, "blockReeds");
        LanguageRegistry.addName(blockReeds, "Resource Reeds");

        blockFertilizedSoil = new BlockFertilizedSoil(Material.grass).setCreativeTab(GrowableResources.tabGrowableResources).setHardness(0.7F).setBlockName("blockFertilizedSoilID");
        blockFertilizedSoil.setHarvestLevel("shovel", 1);
        GameRegistry.registerBlock(blockFertilizedSoil, "blockFertilizedSoil");
        LanguageRegistry.addName(blockFertilizedSoil, "Fertilized Soil");

        blockShredder = new BlockShredder(Material.iron).setCreativeTab(GrowableResources.tabGrowableResources).setHardness(3F).setResistance(2F).setBlockName("blockShredder");
        blockShredder.setHarvestLevel("pickaxe", 2);
        GameRegistry.registerBlock(blockShredder, "blockShredder");
        LanguageRegistry.addName(blockShredder, "Reed Shredder");

        blockInfuser = new BlockInfuser().setCreativeTab(GrowableResources.tabGrowableResources).setHardness(3F).setResistance(2F).setBlockName("blockInfuser");
        blockInfuser.setHarvestLevel("pickaxe", 2);
        GameRegistry.registerBlock(blockInfuser, "blockInfuser");
        LanguageRegistry.addName(blockInfuser, "Reed Infuser");

        blockMiniBlock = new BlockMiniatureBlock().setCreativeTab(GrowableResources.tabGrowableResources).setHardness(2F).setResistance(1.5F).setBlockName("blockMiniBlock");
        GameRegistry.registerBlock(blockMiniBlock, "blockMiniBlock");
        LanguageRegistry.addName(blockMiniBlock, "Miniature Block");
    }

    public static void registerTileEntities() {
        TileEntity.addMapping(TileEntityResourceReeds.class, "gr.resourceReeds");
        TileEntity.addMapping(TileEntityShredder.class, "gr.shredder");
        TileEntity.addMapping(TileEntityInfuser.class, "gr.infuser");
        TileEntity.addMapping(TileEntityMiniatureBlock.class, "gr.miniBlock");
    }
}

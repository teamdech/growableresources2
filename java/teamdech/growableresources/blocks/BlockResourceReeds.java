package teamdech.growableresources.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.IIcon;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import teamdech.growableresources.client.EntityParticleFXReeds;
import teamdech.growableresources.items.Items;
import teamdech.growableresources.proxies.ClientProxy;
import teamdech.growableresources.resources.Resource;
import teamdech.growableresources.resources.Resources;
import teamdech.growableresources.util.GRUtil;

import java.util.ArrayList;
import java.util.Random;

public class BlockResourceReeds extends BlockContainer {
    public static IIcon iconParticle, iconBase, iconOverlay;

    public BlockResourceReeds() {
        super(Material.plants);
        setStepSound(Block.soundTypeGrass);
        setTickRandomly(true);
        setBlockBounds(0.125F, 0.0F, 0.125F, 0.875F, 1.0F, 0.875F);
    }

    @Override
    public IIcon getIcon(int side, int meta) {
        return Blocks.reeds.getIcon(side, meta);
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World par1World, int par2, int par3, int par4) {
        return null;
    }

    @Override
    public int getRenderType() {
        return ClientProxy.reedRendererID;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    public boolean canBlockStayAt(World world, int x, int y, int z, int resourceID) {
        TileEntity tileBelow = world.getTileEntity(x, y - 1, z);
        return world.getBlock(x, y - 1, z) == teamdech.growableresources.blocks.Blocks.blockFertilizedSoil || tileBelow != null && tileBelow instanceof TileEntityResourceReeds && resourceID == ((TileEntityResourceReeds) tileBelow).resourceID;
    }


    @Override
    public void onNeighborBlockChange(World world, int x, int y, int z, Block neighbor) {
        if (!canBlockStayAt(world, x, y, z, ((TileEntityResourceReeds) world.getTileEntity(x, y, z)).resourceID)) {
            removedByPlayer(world, null, x, y, z);
        }
    }

    @Override
    public void registerBlockIcons(IIconRegister iconRegister) {
        iconParticle = iconRegister.registerIcon("growableresources:particle");
        iconBase = iconRegister.registerIcon("growableresources:baseReed");
        iconOverlay = iconRegister.registerIcon("growableresources:baseOverlay");
    }

    @Override
    public ItemStack getPickBlock(MovingObjectPosition target, World world, int x, int y, int z) {
        return new ItemStack(world.getBlockMetadata(x, y, z) == 15 ? Items.itemReeds : Items.itemReedRoots);
    }

    @Override
    public int getDamageValue(World world, int x, int y, int z) {
        return ((TileEntityResourceReeds) world.getTileEntity(x, y, z)).resourceID;
    }

    @Override
    public boolean removedByPlayer(World world, EntityPlayer player, int x, int y, int z) {
        TileEntityResourceReeds tileReeds = (TileEntityResourceReeds) world.getTileEntity(x, y, z);
        if (tileReeds == null) return true;
        int meta = world.getBlockMetadata(x, y, z);
        super.removedByPlayer(world, player, x, y, z);
        if (!world.isRemote && (player == null || !player.capabilities.isCreativeMode))
            GRUtil.spawnItemInWorld(world, new ItemStack(meta != 15 ? Items.itemReedRoots : Items.itemReeds, 1, tileReeds.resourceID), x, y, z, 0, 0);
        world.removeTileEntity(x, y, z);
        return true;

    }

    @Override
    public void updateTick(World world, int x, int y, int z, Random rand) {
        if (!world.isRemote && world.getBlockMetadata(x, y, z) != 15) {
            if (world.getBlockMetadata(x, y, z) == 14) {
                if (world.isAirBlock(x, y + 1, z)) {
                    world.setBlock(x, y + 1, z, this, 15, 3);
                    ((TileEntityResourceReeds) world.getTileEntity(x, y + 1, z)).resourceID = ((TileEntityResourceReeds) world.getTileEntity(x, y, z)).resourceID;
                    world.markBlockForUpdate(x, y + 1, z);
                    world.setBlockMetadataWithNotify(x, y, z, 0, 3);
                } else if (world.isAirBlock(x, y + 2, z)) {
                    world.setBlock(x, y + 2, z, this, 15, 3);
                    ((TileEntityResourceReeds) world.getTileEntity(x, y + 2, z)).resourceID = ((TileEntityResourceReeds) world.getTileEntity(x, y, z)).resourceID;
                    world.markBlockForUpdate(x, y + 2, z);
                    world.setBlockMetadataWithNotify(x, y, z, 0, 3);
                }
            } else {
                world.setBlockMetadataWithNotify(x, y, z, world.getBlockMetadata(x, y, z) + 1, 3);
            }
        }
    }

    @Override
    public ArrayList<ItemStack> getDrops(World world, int x, int y, int z, int metadata, int fortune) {
        return new ArrayList<ItemStack>();
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void randomDisplayTick(World world, int x, int y, int z, Random rand) {
        Resource resource = Resources.getResourceByID(((TileEntityResourceReeds) world.getTileEntity(x, y, z)).resourceID);
        if (rand.nextInt(2) == 0)
            Minecraft.getMinecraft().effectRenderer.addEffect(new EntityParticleFXReeds(world, x + rand.nextFloat(), y + rand.nextFloat(), z + rand.nextFloat(), resource, resource.id == 47));
    }

    @Override
    public TileEntity createNewTileEntity(World world, int meta) {
        return new TileEntityResourceReeds();
    }
}

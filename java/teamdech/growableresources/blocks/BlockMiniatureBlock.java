package teamdech.growableresources.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.IIcon;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import teamdech.growableresources.proxies.ClientProxy;

public class BlockMiniatureBlock extends BlockContainer {

    public BlockMiniatureBlock() {
        super(Material.circuits);
        setBlockBounds(0.3F, 0F, 0.3F, 0.7F, 0.4F, 0.7F);
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int x, int y, int z) {
        return null;
    }

    @Override
    public IIcon getIcon(int i, int j) {
        return net.minecraft.init.Blocks.stone.getIcon(i, j);
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public int isProvidingWeakPower(IBlockAccess world, int x, int y, int z, int side) {
        return ((TileEntityMiniatureBlock) world.getTileEntity(x, y, z)).blockName.equals(Blocks.redstone_block.getUnlocalizedName()) ? 15 : 0;
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
        if (player.isSneaking())
            return false;

        ItemStack test = player.getCurrentEquippedItem();
        if (test != null && test.getItem() instanceof ItemBlock) {
            Block b = ((ItemBlock) test.getItem()).field_150939_a; //Todo test
            if ((b.getRenderType() == 0 || b.getRenderType() == 16 || b.getRenderType() == 31 || b.getRenderType() == 39) && b.getBlockBoundsMinX() == 0F && b.getBlockBoundsMinY() == 0F && b.getBlockBoundsMinZ() == 0F && b.getBlockBoundsMaxX() == 1F && b.getBlockBoundsMaxY() == 1F && b.getBlockBoundsMaxZ() == 1F) {
                TileEntityMiniatureBlock tile = (TileEntityMiniatureBlock) world.getTileEntity(x, y, z);

                if (!world.isRemote) {
                    tile.blockName = test.getUnlocalizedName();
                    tile.damage = test.getItemDamage();
                }

                world.markBlockForUpdate(x, y, z);

                world.updateLightByType(EnumSkyBlock.Block, x, y, z);
                world.updateLightByType(EnumSkyBlock.Sky, x, y, z);

                System.out.println((world.isRemote ? "Client: " : "Server: ") + world.getBlockLightValue(x, y, z));

                world.notifyBlocksOfNeighborChange(x, y, z, this);

                world.notifyBlocksOfNeighborChange(x + 1, y, z, this);
                world.notifyBlocksOfNeighborChange(x - 1, y, z, this);
                world.notifyBlocksOfNeighborChange(x, y + 1, z, this);
                world.notifyBlocksOfNeighborChange(x, y - 1, z, this);
                world.notifyBlocksOfNeighborChange(x, y, z + 1, this);
                world.notifyBlocksOfNeighborChange(x, y, z - 1, this);
            }
        }

        return true;
    }

    @Override //TODO get correct lightvalue
    public int getLightValue(IBlockAccess world, int x, int y, int z) {
        return 0; /*Block.lightValue[((TileEntityMiniatureBlock) world.getTileEntity(x, y, z)).blockID] / 2;*/
    }

    @Override
    public int getRenderType() {
        return ClientProxy.miniBlockRendererID;
    }

    @Override
    public boolean canProvidePower() {
        return true;
    }

    @Override
    public int isProvidingStrongPower(IBlockAccess world, int x, int y, int z, int side) {
        return ((TileEntityMiniatureBlock) world.getTileEntity(x, y, z)).blockName.equals(Blocks.redstone_block.getUnlocalizedName()) ? 15 : 0;
    }

    @Override
    public int onBlockPlaced(World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ, int meta) {
        world.markBlockForUpdate(x, y, z);
        return meta;
    }

    @Override
    public TileEntity createNewTileEntity(World world, int i) {
        return new TileEntityMiniatureBlock();
    }
}

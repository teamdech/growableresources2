package teamdech.growableresources.blocks;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import teamdech.growableresources.GrowableResources;
import teamdech.growableresources.util.GRUtil;

public class BlockInfuser extends BlockContainer {
    public IIcon[] textures = new IIcon[3];

    public BlockInfuser() {
        super(Material.iron);
    }

    @Override
    public IIcon getIcon(int side, int meta) {
        return side == 0 ? textures[0] : (side == 1 ? textures[1] : textures[2]);
    }

    @Override
    public void registerBlockIcons(IIconRegister iconRegister) {
        textures[0] = iconRegister.registerIcon("growableresources:infuser_bottom");
        textures[1] = iconRegister.registerIcon("growableresources:infuser_top");
        textures[2] = iconRegister.registerIcon("growableresources:infuser_sides");
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9) {
        if (!world.isRemote) player.openGui(GrowableResources.INSTANCE, 1, world, x, y, z);
        return true;
    }

    @Override
    public boolean removedByPlayer(World world, EntityPlayer player, int x, int y, int z) {
        TileEntityInfuser tileInfuser = (TileEntityInfuser) world.getTileEntity(x, y, z);
        if (tileInfuser == null) return true;
        if (!world.isRemote && (player == null || !player.capabilities.isCreativeMode))
            for (int i = 0; i < tileInfuser.getSizeInventory(); i++)
                if (tileInfuser.getStackInSlot(i) != null)
                    GRUtil.spawnItemInWorld(world, tileInfuser.getStackInSlot(i), x, y, z, 0, 0);
        super.removedByPlayer(world, player, x, y, z);
        return true;

    }

    @Override
    public TileEntity createNewTileEntity(World world, int i) {
        return new TileEntityInfuser();
    }
}

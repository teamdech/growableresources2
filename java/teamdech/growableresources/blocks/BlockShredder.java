package teamdech.growableresources.blocks;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import teamdech.growableresources.GrowableResources;
import teamdech.growableresources.util.GRUtil;

public class BlockShredder extends BlockContainer {
    IIcon[] textures = new IIcon[3];

    public BlockShredder(Material material) {
        super(material);
    }

//   TODO? Fix BlockShredder Texture
// @Override
//    public IIcon getBlockTexture(IBlockAccess world, int x, int y, int z, int side) {
//        return side == 1 ? textures[0] : (side == 0 ? textures[1] : (world.getBlockMetadata(x, y, z) == side - 2 ? textures[2] : textures[1]));
//    }

    @Override
    public IIcon getIcon(int side, int meta) {
        return side == 1 ? textures[0] : (side == 0 ? textures[1] : (side == 3 ? textures[2] : textures[1]));
    }

    @Override
    public void registerBlockIcons(IIconRegister iconRegister) {
        textures[0] = iconRegister.registerIcon("growableresources:shredder_top");
        textures[1] = iconRegister.registerIcon("growableresources:shredder_blank");
        textures[2] = iconRegister.registerIcon("growableresources:shredder_front");
    }

    @Override
    public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase player, ItemStack stack) {
        if (!world.isRemote)
            world.setBlockMetadataWithNotify(x, y, z, GRUtil.getMetaForRenderingFront(GRUtil.getDirectionFromYaw(player.rotationYaw)), 3);
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9) {
        if (!world.isRemote) player.openGui(GrowableResources.INSTANCE, 0, world, x, y, z);

        return true;
    }

    @Override
    public boolean removedByPlayer(World world, EntityPlayer player, int x, int y, int z) {
        TileEntityShredder tileShredder = (TileEntityShredder) world.getTileEntity(x, y, z);
        if (tileShredder == null) return true;

        super.removedByPlayer(world, player, x, y, z);
        if (!world.isRemote && (player == null || !player.capabilities.isCreativeMode)) for (int i = 0; i < 3; i++)
            if (tileShredder.getStackInSlot(i) != null)
                GRUtil.spawnItemInWorld(world, tileShredder.getStackInSlot(i), x, y, z, 0, 0);
        return true;
    }

    @Override
    public TileEntity createNewTileEntity(World world, int i) {
        return new TileEntityShredder();
    }
}

package teamdech.growableresources.blocks;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityFurnace;
import teamdech.growableresources.items.Items;
import teamdech.growableresources.resources.Resources;
import teamdech.growableresources.util.GRUtil;

public class TileEntityShredder extends TileEntity implements ISidedInventory {
    public int currentFuelLevel, maxFuelLevel = 1600;
    public int currentProgress;

    public ItemStack inputSlot, fuelSlot, outputSlot;

    private static final int[] slotsTop = new int[]{0, 1};
    private static final int[] slotBottom = new int[]{2};
    private static final int[] slotsSides = new int[]{1};

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        currentFuelLevel = compound.getInteger("currentFuelLevel");
        maxFuelLevel = compound.getInteger("maxFuelLevel");
        currentProgress = compound.getInteger("currentProgress");

        GRUtil.loadInventory(compound, this);
    }

    @Override
    public void writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        compound.setInteger("currentFuelLevel", currentFuelLevel);
        compound.setInteger("maxFuelLevel", maxFuelLevel);
        compound.setInteger("currentProgress", currentProgress);

        GRUtil.saveInventory(compound, this);
    }

    @Override
    public void updateEntity() {
        if (worldObj.isRemote) return;

        ItemStack stackInOutputSlot = getStackInSlot(2), stackInInputSlot = getStackInSlot(0), stackInFuelSlot = getStackInSlot(1);
        boolean canProcess = stackInInputSlot != null && (stackInOutputSlot == null || (stackInOutputSlot.getItem() == Resources.getResourceByID(stackInInputSlot.getItemDamage()).product.getItem() && stackInOutputSlot.getItemDamage() == Resources.getResourceByID(stackInInputSlot.getItemDamage()).product.getItemDamage() && stackInOutputSlot.stackSize < stackInOutputSlot.getItem().getItemStackLimit(stackInOutputSlot)));

        if (currentFuelLevel > 0 && canProcess) {
            if (currentProgress == 199) {
                currentProgress = 0;
                processItem(stackInOutputSlot, stackInInputSlot, Resources.getResourceByID(stackInInputSlot.getItemDamage()).product);
            } else {
                currentFuelLevel--;
                currentProgress++;
            }
        } else if (currentFuelLevel > 0 && !canProcess) {
            currentFuelLevel--;
            if (currentProgress > 0) currentProgress = 0;
        } else if (currentFuelLevel == 0 && canProcess && TileEntityFurnace.isItemFuel(stackInFuelSlot)) {
            currentFuelLevel += TileEntityFurnace.getItemBurnTime(stackInFuelSlot);
            maxFuelLevel = currentFuelLevel;
            if (stackInFuelSlot.stackSize > 1) stackInFuelSlot.stackSize--;
            else setInventorySlotContents(1, null);
        } else if (currentFuelLevel == 0 && !TileEntityFurnace.isItemFuel(stackInFuelSlot) && currentProgress > 0) {
            currentProgress = 0;
        }
    }

    private void processItem(ItemStack stackInOutputSlot, ItemStack stackInInputSlot, ItemStack product) {
        setInventorySlotContents(2, new ItemStack(product.getItem(), (stackInOutputSlot == null ? 1 : stackInOutputSlot.stackSize + 1), product.getItemDamage()));
        if (stackInInputSlot.stackSize > 1) stackInInputSlot.stackSize--;
        else setInventorySlotContents(0, null);
    }

    @Override
    public int getSizeInventory() {
        return 3;
    }

    @Override
    public ItemStack getStackInSlot(int slotID) {
        return slotID == 0 ? inputSlot : (slotID == 1 ? fuelSlot : outputSlot);
    }

    @Override
    public ItemStack decrStackSize(int slotID, int amount) {
        return GRUtil.decrStackSize(slotID, amount, this);
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int slotID) {
        return GRUtil.stackInSlotOnClosing(slotID, this);
    }

    @Override
    public void setInventorySlotContents(int slotID, ItemStack stack) {
        if (stack != null && stack.stackSize > getInventoryStackLimit()) {
            stack.stackSize = getInventoryStackLimit();
        }

        if (slotID == 0) inputSlot = stack;
        else if (slotID == 1) fuelSlot = stack;
        else if (slotID == 2) outputSlot = stack;
    }

    @Override
    public String getInventoryName() {
        return null;
    }

    @Override
    public boolean hasCustomInventoryName() {
        return false;
    }

    @Override
    public int getInventoryStackLimit() {
        return 64;
    }

    @Override
    public boolean isUseableByPlayer(EntityPlayer entityplayer) {
        return entityplayer.getDistanceSq(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5) <= 64;
    }

    @Override
    public void openInventory() {
    }

    @Override
    public void closeInventory() {
    }

    @Override
    public boolean isItemValidForSlot(int slotID, ItemStack stack) {
        return (slotID == 0 && stack.getItem() == Items.itemReeds) || (slotID == 1 && TileEntityFurnace.isItemFuel(stack));
    }

    @Override
    public int[] getAccessibleSlotsFromSide(int side) {
        return side == 1 ? slotsTop : (side == 0 ? slotBottom : slotsSides);
    }

    @Override
    public boolean canInsertItem(int slot, ItemStack stack, int side) {
        return side == 1 && slot == 0 ? stack.getItem() == Items.itemReeds : side != 0 && slot == 1 && TileEntityFurnace.isItemFuel(stack);
    }

    @Override
    public boolean canExtractItem(int slot, ItemStack stack, int side) {
        return side == 0 && slot == 2;
    }
}

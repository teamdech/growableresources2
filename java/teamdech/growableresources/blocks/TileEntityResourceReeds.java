package teamdech.growableresources.blocks;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class TileEntityResourceReeds extends TileEntity {
    public int resourceID = 0;

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        resourceID = compound.getInteger("resourceID");
    }

    @Override
    public void writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        compound.setInteger("resourceID", resourceID);
    }
/* TODO PACKET HANDLING */
//    @Override
//    public Packet getDescriptionPacket() {
//        NBTTagCompound data = new NBTTagCompound();
//        writeToNBT(data);
//        return new Packet132TileEntityData(xCoord, yCoord, zCoord, 1, data);
//    }
//    @Override
//    public void onDataPacket(INetworkManager net, Packet132TileEntityData packet) {
//        readFromNBT(packet.data);
//    }
}

package teamdech.growableresources.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import teamdech.growableresources.GrowableResources;

public class BlockFertilizedSoil extends Block {
    public BlockFertilizedSoil(Material material) {
        super(material);
        setCreativeTab(GrowableResources.tabGrowableResources);
        setStepSound(new SoundType("gravel", 1.0F, 0.75F));
    }

}
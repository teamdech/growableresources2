package teamdech.growableresources.blocks;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import teamdech.growableresources.items.Items;
import teamdech.growableresources.recipe.Recipes;
import teamdech.growableresources.util.GRUtil;

public class TileEntityInfuser extends TileEntity implements ISidedInventory {
    public ItemStack[] materials = new ItemStack[8];
    public ItemStack input, fuel, output;

    public boolean locked;
    public int progress;

    private static final int[] slotsTop = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8};
    private static final int[] slotsSides = new int[]{9};
    private static final int[] slotsBottom = new int[]{10};

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        locked = compound.getBoolean("locked");
        progress = compound.getInteger("progress");

        GRUtil.loadInventory(compound, this);
    }

    @Override
    public void writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        compound.setBoolean("locked", locked);
        compound.setInteger("progress", progress);

        GRUtil.saveInventory(compound, this);
    }

    @Override
    public int getSizeInventory() {
        return 11;
    }

    @Override
    public void updateEntity() {
        if (worldObj.isRemote) return;

        if (locked && progress == 1200) {
            progress = 0;
            locked = false;
            ItemStack[] materials = new ItemStack[8];
            for (int i = 0; i < 8; i++) materials[i] = getStackInSlot(i + 1);
            setInventorySlotContents(10, Recipes.infusionCrafting.getOutput(getStackInSlot(8), materials));
            for (int i = 0; i < 10; i++) decrStackSize(i, 1);
        } else if (locked) {
            progress++;
        } else {
            if (getStackInSlot(8) != null && getStackInSlot(9) != null && getStackInSlot(10) == null) {
                locked = true;
            }
        }
    }

    @Override
    public ItemStack getStackInSlot(int slotID) {
        return slotID < 8 ? materials[slotID] : (slotID == 8 ? input : (slotID == 9 ? fuel : output));
    }

    @Override
    public ItemStack decrStackSize(int slotID, int amount) {
        return locked ? null : GRUtil.decrStackSize(slotID, amount, this);
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int slotID) {
        return GRUtil.stackInSlotOnClosing(slotID, this);
    }

    @Override
    public int getInventoryStackLimit() {
        return 64;
    }

    @Override
    public boolean isUseableByPlayer(EntityPlayer entityplayer) {
        return entityplayer.getDistanceSq(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5) <= 64;
    }

    @Override
    public void openInventory() {

    }

    @Override
    public void closeInventory() {

    }

    @Override
    public boolean isItemValidForSlot(int slotID, ItemStack stack) {
        return (slotID >= 0 && slotID < 8) || (slotID == 8 && stack.getItem() == Items.itemReedRoots) || (slotID == 9 && stack.getItem() == Items.itemInfusionFuel);
    }

    @Override
    public void setInventorySlotContents(int slotID, ItemStack stack) {
        if (stack != null && stack.stackSize > getInventoryStackLimit()) {
            stack.stackSize = getInventoryStackLimit();
        }

        if (slotID >= 0 && slotID < 8) materials[slotID] = stack;
        else if (slotID == 8) input = stack;
        else if (slotID == 9) fuel = stack;
        else if (slotID == 10) output = stack;
    }

    @Override
    public String getInventoryName() {
        return null;
    }

    @Override
    public boolean hasCustomInventoryName() {
        return false;
    }

    @Override
    public int[] getAccessibleSlotsFromSide(int side) {
        return side == 1 ? slotsTop : (side == 0 ? slotsBottom : slotsSides);
    }

    @Override
    public boolean canInsertItem(int slotID, ItemStack stack, int side) {
        return !locked && (side != 0 && (side != 1 && slotID == 9 && stack.getItem() == Items.itemInfusionFuel ? true : slotID < 9));
    }

    @Override
    public boolean canExtractItem(int slotID, ItemStack stack, int side) {
        return side == 0 && slotID == 10;
    }
}

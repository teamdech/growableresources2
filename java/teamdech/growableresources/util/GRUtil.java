package teamdech.growableresources.util;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.oredict.OreDictionary;

public class GRUtil {
    public static void spawnItemInWorld(World world, ItemStack item, double x, double y, double z, int pickupTime, int lifetime) {
        float f = 0.7F;
        double d0 = (double) (world.rand.nextFloat() * f) + (double) (1.0F - f) * 0.5D;
        double d1 = (double) (world.rand.nextFloat() * f) + (double) (1.0F - f) * 0.5D;
        double d2 = (double) (world.rand.nextFloat() * f) + (double) (1.0F - f) * 0.5D;

        EntityItem entityItem = new EntityItem(world, x + d0, y + d1, z + d2, item);

        entityItem.delayBeforeCanPickup = pickupTime == 0 ? 10 : pickupTime;
        entityItem.lifespan = lifetime == 0 ? 6000 : lifetime;

        world.spawnEntityInWorld(entityItem);
    }

    public static int getDirectionFromYaw(float yaw) {
        return MathHelper.floor_double((double) (yaw * 4.0F / 360.0F) + 0.5D) & 3;
    }

    public static int getMetaForRenderingFront(int direction) {
        return direction == 0 ? 0 : (direction == 1 ? 3 : (direction == 2 ? 1 : 2));
    }

    public static void saveInventory(NBTTagCompound compound, IInventory inventory) {
        NBTTagList items = new NBTTagList();

        for (int i = 0; i < inventory.getSizeInventory(); i++) {
            ItemStack stack = inventory.getStackInSlot(i);

            if (stack != null) {
                NBTTagCompound item = new NBTTagCompound();
                item.setByte("Slot", (byte) i);
                stack.writeToNBT(item);
                items.appendTag(item);
            }
        }

        compound.setTag("Items", items);
    }

    public static void loadInventory(NBTTagCompound compound, IInventory inventory) {
        NBTTagList items = compound.getTagList("Items", Constants.NBT.TAG_COMPOUND);

        for (int i = 0; items.tagCount() > i; i++) {
            NBTTagCompound item = items.getCompoundTagAt(i);
            int slot = item.getByte("Slot");

            if (slot >= 0 && slot < inventory.getSizeInventory()) {
                inventory.setInventorySlotContents(slot, ItemStack.loadItemStackFromNBT(item));
            }
        }
    }

    public static ItemStack decrStackSize(int slotID, int amount, IInventory inventory) {
        ItemStack stack = inventory.getStackInSlot(slotID);

        if (stack != null) {
            ItemStack temp;
            temp = stack.copy();

            if (amount >= stack.stackSize) {
                inventory.setInventorySlotContents(slotID, null);

                temp.stackSize = stack.stackSize;
            } else {
                inventory.setInventorySlotContents(slotID, new ItemStack(stack.getItem(), stack.stackSize - amount, stack.getItemDamage()));
                temp.stackSize = amount;
            }

            return temp;
        }
        return null;
    }

    public static ItemStack stackInSlotOnClosing(int slotID, IInventory inventory) {
        ItemStack returnStack = inventory.getStackInSlot(slotID);
        inventory.setInventorySlotContents(slotID, null);
        return returnStack;
    }

    public static boolean hasOreDictEntries(String oreDictName) {
        return OreDictionary.getOres(oreDictName).size() > 0;
    }

    public static ItemStack getFirstStack(String oreDictName) {
        return hasOreDictEntries(oreDictName) ? OreDictionary.getOres(oreDictName).get(0) : null;
    }


    /**
     * Gets the int colorvalue
     *
     * @param r Red
     * @param g Green
     * @param b Blue
     * @return int colorvalue
     */
    public static int getColorInt(int r, int g, int b) {
        String red = Integer.toHexString(r);
        String green = Integer.toHexString(g);
        String blue = Integer.toHexString(b);

        if (red.length() == 1) red = 0 + red;
        if (green.length() == 1) green = 0 + green;
        if (blue.length() == 1) blue = 0 + blue;

        return Integer.parseInt(red + green + blue, 16);
    }

    /**
     * Gets the int colorvalue, <i><b>0.0F => 0 | 1.0F => 255</b></i>
     *
     * @param r Red
     * @param g Green
     * @param b Blue
     * @return int colorvalue
     */
    public static int getColorInt(float r, float g, float b) {
        String red = Integer.toHexString((int) (r * 255F));
        String green = Integer.toHexString((int) (g * 255F));
        String blue = Integer.toHexString((int) (b * 255F));

        if (red.length() == 1) red = 0 + red;
        if (green.length() == 1) green = 0 + green;
        if (blue.length() == 1) blue = 0 + blue;

        return Integer.parseInt(red + green + blue, 16);
    }
}

package teamdech.growableresources.util;

import net.minecraftforge.common.config.Configuration;

import java.io.File;

public class GRConfig {
    public static int tickDeviceTicksPerUse = 1;
    public static boolean enableParticleFX = true, enableColoredNames = true, enableOverpoweredFeatures = false;

    private Configuration configuration;

    public GRConfig loadConfig(File file) {
        configuration = new Configuration(file);

        enableParticleFX = configuration.get("Visual", "EnableParticleFX", true).getBoolean(true);
        enableColoredNames = configuration.get("Visual", "EnableColoredNames", true).getBoolean(true);

        tickDeviceTicksPerUse = configuration.get("OP", "tickingDeviceTicksPerUse", 1).getInt(1);
        enableOverpoweredFeatures = configuration.get("OP", "enableOverpoweredFeatures", false).getBoolean(false);

        return this;
    }

    public void saveConfig() {
        configuration.save();
    }
}

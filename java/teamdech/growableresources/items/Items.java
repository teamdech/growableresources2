package teamdech.growableresources.items;

import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.item.Item;
import teamdech.growableresources.GrowableResources;

public class Items {
    public static Item itemReeds, itemReedRoots, itemInfusionFuel, itemInfusionCore, itemTicker, itemGuide, itemInfoTool;

    public static void initializeItems() {
        itemReedRoots = new ItemReed().setCreativeTab(GrowableResources.tabGrowableResources).setUnlocalizedName("itemReedRoots");
        itemReeds = new ItemReed().setCreativeTab(GrowableResources.tabGrowableResources).setUnlocalizedName("itemReeds");
        itemInfusionFuel = new Item().setCreativeTab(GrowableResources.tabGrowableResources).setTextureName("growableresources:infusionFuel").setUnlocalizedName("itemInfusionFuel");
        itemInfusionCore = new Item().setCreativeTab(GrowableResources.tabGrowableResources).setTextureName("growableresources:infusionCore").setUnlocalizedName("itemInfusionCore");
        itemTicker = new ItemTicker().setCreativeTab(GrowableResources.tabGrowableResources).setTextureName("growableresources:ticker").setUnlocalizedName("itemTicker");
        itemGuide = new ItemGuide().setCreativeTab(GrowableResources.tabGrowableResources);
        itemInfoTool = new ItemInfoTool().setCreativeTab(GrowableResources.tabGrowableResources).setTextureName("growableresources:infoTool").setUnlocalizedName("itemInfoTool");

        LanguageRegistry.addName(itemReedRoots, "Resource Reed Roots");
        LanguageRegistry.addName(itemReeds, "Resource Reeds");
        LanguageRegistry.addName(itemInfusionFuel, "Infusion Fuel");
        LanguageRegistry.addName(itemInfusionCore, "Infusion Core");
        LanguageRegistry.addName(itemTicker, "The Allmighty Ticking Device");
        LanguageRegistry.addName(itemGuide, "Guide");
        LanguageRegistry.addName(itemInfoTool, "Info Tool");
    }
}
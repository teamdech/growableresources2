package teamdech.growableresources.items;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import teamdech.growableresources.blocks.Blocks;
import teamdech.growableresources.blocks.TileEntityResourceReeds;
import teamdech.growableresources.resources.Resource;
import teamdech.growableresources.resources.Resources;
import teamdech.growableresources.util.GRUtil;

import java.util.List;

public class ItemReed extends Item {
    public IIcon overlay;

    public ItemReed() {
        super();
        setHasSubtypes(true);
    }

    @Override
    public void registerIcons(IIconRegister iconRegister) {
        itemIcon = iconRegister.registerIcon(this == Items.itemReeds ? "growableresources:reeds" : "growableresources:reedroots");
        overlay = iconRegister.registerIcon("growableresources:overlay");
    }

    @Override
    public void getSubItems(Item item, CreativeTabs tab, List list) {
        for (int i = 0; i < Resources.getNextAvailableID(); i++) {
            list.add(new ItemStack(item, 1, i));
        }
    }

    @Override
    public EnumRarity getRarity(ItemStack stack) {
        return Resources.getResourceByID(stack.getItemDamage()).rarity;
    }

    @Override
    public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b) {
        list.add(Resources.getResourceByID(stack.getItemDamage()).name);
    }

    @Override
    public boolean requiresMultipleRenderPasses() {
        return this == Items.itemReeds ? true : false;
    }

    @Override
    public IIcon getIconFromDamageForRenderPass(int damage, int pass) {
        return pass == 0 ? itemIcon : overlay;
    }

    @Override
    public boolean onItemUse(ItemStack itemStack, EntityPlayer player, World world, int x, int y, int z, int i, float hitX, float hitY, float hitZ) {
        if (!player.canPlayerEdit(x, y, z, i, itemStack)) return false;
        else if (itemStack.stackSize == 0) return false;
        else if (itemStack.getItem() != Items.itemReedRoots) return false;

        Block block = world.getBlock(x, y, z);

        if (block == net.minecraft.init.Blocks.snow_layer && (world.getBlockMetadata(x, y, z) & 7) < 1) i = 1;
        else if (block != net.minecraft.init.Blocks.vine && block != net.minecraft.init.Blocks.tallgrass && block != net.minecraft.init.Blocks.deadbush) {
            if (i == 0) {
                --y;
            }

            if (i == 1) {
                ++y;
            }

            if (i == 2) {
                --z;
            }

            if (i == 3) {
                ++z;
            }

            if (i == 4) {
                --x;
            }

            if (i == 5) {
                ++x;
            }
        }

        block = world.getBlock(x, y, z);

        if (world.getBlock(x, y - 1, z) == Blocks.blockFertilizedSoil && (world.isAirBlock(x, y, z) || block.isReplaceable(world, x, y, z))) {
            world.setBlock(x, y, z, Blocks.blockReeds);

            TileEntityResourceReeds newTile = (TileEntityResourceReeds) world.getTileEntity(x, y, z);

            newTile.resourceID = itemStack.getItemDamage();

            world.playSoundEffect((double) ((float) x + 0.5F), (double) ((float) y + 0.5F), (double) ((float) z + 0.5F), Blocks.blockReeds.stepSound.getBreakSound()/*TODO place sound*/, (Blocks.blockReeds.stepSound.getVolume() + 1.0F) / 2.0F, Blocks.blockReeds.stepSound.getPitch() * 0.8F);
            --itemStack.stackSize;

            world.markBlockForUpdate(x, y, z);

            return true;
        }
        return false;
    }

    @Override
    public int getColorFromItemStack(ItemStack stack, int pass) {
        Resource r = Resources.getResourceByID(stack.getItemDamage());

        return (pass == 0 ? 0xFFFFFF : GRUtil.getColorInt(r.r, r.g, r.b));
    }

    @Override
    public void onUpdate(ItemStack itemStack, World world, Entity entity, int par4, boolean par5) {
        super.onUpdate(itemStack, world, entity, par4, par5);

        if (!Resources.getResourceByID(itemStack.getItemDamage()).active) {
            itemStack.stackSize = 0;
            itemStack = null;
        }
    }
}
package teamdech.growableresources.items;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import teamdech.growableresources.GrowableResources;

/**
 * User: joel / Date: 16.12.13 / Time: 22:17
 */
public class ItemGuide extends Item {

    public ItemGuide() {
        super();
        setUnlocalizedName("itemGuide").setMaxStackSize(1);
    }

    @Override
    public void registerIcons(IIconRegister iconRegister) {
        itemIcon = iconRegister.registerIcon("growableresources:guide");
    }

    @Override
    public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer entityPlayer) {
        entityPlayer.openGui(GrowableResources.INSTANCE, 2, world, 0, 0, 0);
        return super.onItemRightClick(itemStack, world, entityPlayer);
    }
}

package teamdech.growableresources.items;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import teamdech.growableresources.util.GRConfig;

//TODO max stack size to 1
public class ItemTicker extends Item {
    public ItemTicker() {
        super();
    }

    @Override
    public boolean onItemUseFirst(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ) {
        if (!world.isRemote) {
            if (GRConfig.tickDeviceTicksPerUse > 1) {
                for (int i = 1; i <= GRConfig.tickDeviceTicksPerUse; i++) {
                    if (world.getBlock(x, y, z) != null)
                        world.getBlock(x, y, z).updateTick(world, x, y, z, world.rand);
                }
            } else if (world.getBlock(x, y, z) != null) {
                world.getBlock(x, y, z).updateTick(world, x, y, z, world.rand);
            }
        }

        return false;
    }

    @Override
    public EnumRarity getRarity(ItemStack stack) {
        return EnumRarity.epic;
    }
}

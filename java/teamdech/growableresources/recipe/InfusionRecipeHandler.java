package teamdech.growableresources.recipe;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import teamdech.growableresources.resources.Resource;
import teamdech.growableresources.resources.Resources;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class InfusionRecipeHandler {
    public int[] infusionBaseValues;
    public ArrayList<Material> materials;

    public InfusionRecipeHandler() {
        infusionBaseValues = new int[Resources.getNextAvailableID()];

        for (int i = 0; i < Resources.getNextAvailableID(); i++) {
            infusionBaseValues[i] = Resources.getResourceByID(i).infusionBaseValue;
        }

        materials = new ArrayList<Material>();

        add(1, 5, new ItemStack(Items.iron_ingot));
        add(1, 50, new ItemStack(Blocks.iron_block));
        add(2, 5, new ItemStack(Items.gold_ingot));
        add(2, 50, new ItemStack(Blocks.gold_block));
        add(3, 4, new ItemStack(Items.diamond));
        add(3, 42, new ItemStack(Blocks.diamond_block));
        add(4, 4, new ItemStack(Items.emerald));
        add(4, 40, new ItemStack(Blocks.emerald_block));
        add(5, 8, new ItemStack(Items.dye, 1, 4));
        add(5, 75, new ItemStack(Blocks.lapis_block));
        add(6, 10, new ItemStack(Items.glowstone_dust));
        add(6, 45, new ItemStack(Blocks.glowstone));
        add(6, 3, new ItemStack(Blocks.netherrack));
        add(7, 15, new ItemStack(Items.clay_ball));
        add(7, 60, new ItemStack(Blocks.clay));
        add(7, 40, new ItemStack(Blocks.hardened_clay));
        add(7, 5, new ItemStack(Blocks.sand));
        add(8, 10, new ItemStack(Items.quartz));
        add(8, 45, new ItemStack(Blocks.quartz_block));
        add(8, 3, new ItemStack(Blocks.netherrack));
        add(9, 10, new ItemStack(Blocks.brown_mushroom_block));
        add(9, 50, new ItemStack(Blocks.mycelium));
        add(10, 10, new ItemStack(Blocks.red_mushroom_block));
        add(10, 50, new ItemStack(Blocks.mycelium));
        add(11, 5, new ItemStack(Items.ender_pearl));
        add(11, 2, new ItemStack(Blocks.obsidian));
        add(12, 8, new ItemStack(Items.blaze_rod));
        add(12, 4, new ItemStack(Items.blaze_powder));
        add(13, 5, new ItemStack(Items.skull, 1, 1));
        add(13, 2, new ItemStack(Items.coal));
        add(13, 2, new ItemStack(Items.bone));
        add(14, 10, new ItemStack(Items.cookie));
        add(15, 7, new ItemStack(Items.slime_ball));
        add(16, 10, new ItemStack(Items.potato));
        add(16, 13, new ItemStack(Items.baked_potato));
        add(17, 10, new ItemStack(Items.carrot));
        add(18, 12, new ItemStack(Items.wheat));
        add(18, 80, new ItemStack(Blocks.hay_block));
        add(19, 8, new ItemStack(Items.ghast_tear));
        add(19, 1, new ItemStack(Blocks.netherrack));
        add(20, 10, new ItemStack(Items.leather));
        add(20, 7, new ItemStack(Items.beef));
        add(21, 6, new ItemStack(Items.experience_bottle));
        add(21, 1, new ItemStack(Blocks.bookshelf));
        add(22, 10, new ItemStack(Items.egg));
        add(23, 12, new ItemStack(Items.apple));
        add(23, 80, new ItemStack(Items.golden_apple));
        add(24, 6, new ItemStack(Blocks.obsidian));
        add(24, 16, new ItemStack(Items.ender_pearl));
        add(25, 15, new ItemStack(Blocks.soul_sand));
        add(25, 4, new ItemStack(Blocks.netherrack));
        add(26, 8, new ItemStack(Blocks.wool));
        add(27, 8, new ItemStack(Items.dye));
        add(27, 16, new ItemStack(Blocks.obsidian));
        add(28, 10, new ItemStack(Items.string));
        add(28, 45, new ItemStack(Blocks.wool));
        add(29, 12, new ItemStack(Items.redstone));
        add(29, 120, new ItemStack(Blocks.redstone_block));
        add(29, 50, new ItemStack(Items.repeater));
        add(30, 10, new ItemStack(Items.porkchop));
        add(30, 12, new ItemStack(Items.cooked_porkchop));
        add(31, 10, new ItemStack(Items.beef));
        add(31, 12, new ItemStack(Items.cooked_beef));
        add(31, 15, new ItemStack(Items.leather));
        add(32, 10, new ItemStack(Items.chicken));
        add(32, 12, new ItemStack(Items.cooked_chicken));
        add(32, 15, new ItemStack(Items.egg));
        add(33, 6, new ItemStack(Items.gunpowder));
        add(33, 4, new ItemStack(Blocks.obsidian));
        add(34, 4, new ItemStack(Items.coal));
        add(34, 45, new ItemStack(Blocks.coal_block));
        add(34, 3, new ItemStack(Items.coal, 1, 1));
    }

    public ItemStack getOutput(ItemStack input, ItemStack[] materials) {
        ArrayList<Integer> results = new ArrayList<Integer>();

        if (input == null) return null;

        for (int i = 0; i < Resources.getResourceByID(input.getItemDamage()).infusionBaseValue; i++) {
            results.add(input.getItemDamage());
        }

        Iterator<Material> i = Recipes.infusionCrafting.materials.iterator();
        while (i.hasNext()) {
            Material temp = i.next();

            for (int j = 0; j < 8; j++) {
                if (materials[j] != null) {
                    if (temp.item == materials[j].getItem() && temp.meta == materials[j].getItemDamage() && validMaterial(temp, input.getItemDamage())) {
                        for (int k = 0; k < temp.possibility; k++) {
                            results.add(temp.product);
                        }
                    }
                }
            }
        }

        int len = results.toArray().length;
        Integer[] resourceIDs = new Integer[len];
        resourceIDs = results.toArray(resourceIDs);
        int resourceID = resourceIDs[new Random().nextInt(len)];
        return new ItemStack(teamdech.growableresources.items.Items.itemReedRoots, 1, resourceID);
    }

    public void add(int product, int probability, ItemStack stack) {
        materials.add(new Material(product, probability, stack));
    }

    public boolean validMaterial(Material material, int baseResource) {
        Iterator<Resource> i = Resources.resourceList.iterator();

        while (i.hasNext()) {
            Resource temp = i.next();
            if (material.product == temp.id && temp.origin == baseResource) {
                return true;
            }
        }
        return false;
    }
}

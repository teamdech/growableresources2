package teamdech.growableresources.recipe;

import cpw.mods.fml.common.IFuelHandler;
import net.minecraft.item.ItemStack;
import teamdech.growableresources.items.Items;

/**
 * User: joel / Date: 16.12.13 / Time: 17:09
 */
public class GRFuelHandler implements IFuelHandler {

    @Override
    public int getBurnTime(ItemStack fuel) {
        if (fuel.getItem() == Items.itemInfusionFuel) {
            return 2000;
        }
        return 0;
    }
}

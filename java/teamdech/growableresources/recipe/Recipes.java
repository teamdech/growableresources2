package teamdech.growableresources.recipe;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import teamdech.growableresources.blocks.Blocks;
import teamdech.growableresources.items.Items;

public class Recipes {
    public static InfusionRecipeHandler infusionCrafting;

    public static void addRecipes() {
        CraftingManager.getInstance().addRecipe(new ItemStack(Blocks.blockFertilizedSoil, 2, 0), "BDB", "DWD", "BDB", 'B', new ItemStack(net.minecraft.init.Items.dye, 1, 15), 'D', net.minecraft.init.Blocks.dirt, 'W', net.minecraft.init.Items.water_bucket);
        CraftingManager.getInstance().addRecipe(new ItemStack(Blocks.blockShredder, 1, 0), "SRS", "PIP", "SRS", 'S', net.minecraft.init.Blocks.stone, 'P', net.minecraft.init.Items.diamond_pickaxe, 'I', net.minecraft.init.Blocks.iron_block, 'R', net.minecraft.init.Items.redstone);
        CraftingManager.getInstance().addRecipe(new ItemStack(Blocks.blockInfuser, 1, 0), "BSB", "SIS", "BSB", 'B', net.minecraft.init.Items.blaze_rod, 'S', net.minecraft.init.Blocks.stone, 'I', Items.itemInfusionCore);
        CraftingManager.getInstance().addRecipe(new ItemStack(Blocks.blockMiniBlock, 4, 0), "S", 'S', net.minecraft.init.Blocks.stone_slab);

        CraftingManager.getInstance().addRecipe(new ItemStack(Items.itemInfusionCore, 1, 0), "GDG", "DRD", "GDG", 'G', net.minecraft.init.Items.gold_ingot, 'D', net.minecraft.init.Items.diamond, 'R', Items.itemReedRoots);
        CraftingManager.getInstance().addShapelessRecipe(new ItemStack(Items.itemInfusionFuel, 2, 0), net.minecraft.init.Items.blaze_rod, new ItemStack(net.minecraft.init.Items.coal, 1, Short.MAX_VALUE), net.minecraft.init.Items.gunpowder, net.minecraft.init.Items.blaze_powder);
        CraftingManager.getInstance().addShapelessRecipe(new ItemStack(Items.itemGuide), net.minecraft.init.Items.book, net.minecraft.init.Items.reeds);

        CraftingManager.getInstance().addRecipe(new ItemStack(Items.itemInfoTool), "GGG", "GQG", "SRS", 'G', net.minecraft.init.Blocks.glass_pane, 'Q', net.minecraft.init.Items.quartz, 'S', net.minecraft.init.Blocks.stone, 'R', net.minecraft.init.Items.redstone);

        infusionCrafting = new InfusionRecipeHandler();
        new InfusionTooltipHandler();
        GameRegistry.registerFuelHandler(new GRFuelHandler());
    }
}
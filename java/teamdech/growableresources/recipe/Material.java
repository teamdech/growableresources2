package teamdech.growableresources.recipe;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class Material {
    public int product, possibility, meta;
    public Item item;

    public Material(int product, int possibility, ItemStack stack) {
        this.product = product;
        this.possibility = possibility;
        item = stack.getItem();
        meta = stack.getItemDamage();
    }
}

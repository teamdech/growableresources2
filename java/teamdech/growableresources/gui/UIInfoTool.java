package teamdech.growableresources.gui;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.inventory.Container;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import teamdech.growableresources.blocks.Blocks;
import teamdech.growableresources.blocks.TileEntityResourceReeds;
import teamdech.growableresources.client.ReedRenderer;
import teamdech.growableresources.client.TickHandlerClient;
import teamdech.growableresources.resources.Resource;
import teamdech.growableresources.resources.Resources;
import teamdech.growableresources.util.GRUtil;

/**
 * User: joel / Date: 20.12.13 / Time: 17:31
 */
public class UIInfoTool extends GuiContainer {

    ContainerInfoTool c;
    int metaValue;
    Resource resource;
    String resourceName;
    ResourceLocation reedTexture;

    @SideOnly(Side.CLIENT)
    private final RenderBlocks renderblocks = new RenderBlocks();

    public UIInfoTool(Container container) {
        super(container);
        c = (ContainerInfoTool) container;
        xSize = 256;
        ySize = 128;

        TileEntityResourceReeds tileEntityResourceReeds = (TileEntityResourceReeds) c.world.getTileEntity(c.x, c.y, c.z);
        metaValue = c.world.getBlockMetadata(c.x, c.y, c.z);
        resource = Resources.getResourceByID(tileEntityResourceReeds.resourceID);
        resourceName = resource.name;
        reedTexture = new ResourceLocation("growableresources", "textures/blocks/reed_" + resource.id + ".png");

        renderblocks.blockAccess = Minecraft.getMinecraft().theWorld;
    }

    @Override
    @SideOnly(Side.CLIENT)
    protected void drawGuiContainerBackgroundLayer(float f, int i, int j) {
        GL11.glColor3f(1, 1, 1);

        Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation("growableresources", "textures/gui/guiInfoTool.png"));
        drawTexturedModalRect(guiLeft, guiTop, 0, 0, 256, 128);

        int total = c.world.getBlock(c.x, c.y + 2, c.z) == Blocks.blockReeds && c.world.getBlockMetadata(c.x, c.y + 2, c.z) == 15 ? 30 : c.world.getBlockMetadata(c.x, c.y, c.z) + (c.world.getBlock(c.x, c.y + 1, c.z) == Blocks.blockReeds && c.world.getBlockMetadata(c.x, c.y + 1, c.z) == 15 ? 15 : 0);

        int progress = (int) ((100F / 30F) * (float) total);
        float progressPixels = (249F / 30F) * (float) total;

        drawTexturedModalRect(guiLeft + 3, guiTop + 111, 0, 128, (int) progressPixels, 14);

        fontRendererObj.drawString("Reed:", guiLeft + 6, guiTop + 25, 0xD9D9D9);
        fontRendererObj.drawString(resourceName, guiLeft + 40, guiTop + 25, GRUtil.getColorInt(resource.r, resource.g, resource.b));
        fontRendererObj.drawString("Growth progress:", guiLeft + 6, guiTop + 101, 0xC8C8C8);
        fontRendererObj.drawString(String.format("%s%%", progress), guiLeft + (metaValue != 15 ? 120 : 105), guiTop + 115, 0xFFFFFF);

        GL11.glPushMatrix();
        GL11.glTranslatef(guiLeft + 200, guiTop + 65, 100);
        GL11.glScalef(-30F, 30F, 30F);

        GL11.glRotatef(-25, 1, 0, 0);
        GL11.glRotatef(TickHandlerClient.rotation, 0, 1, 0);
        GL11.glRotatef(180, 0, 0, 1);

        renderblocks.setRenderBoundsFromBlock(Blocks.blockReeds);
        ReedRenderer.renderPreview(Minecraft.getMinecraft().theWorld, 0, 0, 0, resource.id);

        GL11.glPopMatrix();
    }
}
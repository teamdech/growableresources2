package teamdech.growableresources.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class UIShredder extends GuiContainer {
    public ContainerShredder shredder;

    public UIShredder(ContainerShredder container) {
        super(container);

        shredder = container;

        xSize = 176;
        ySize = 166;
    }

    @Override
    public void drawGuiContainerBackgroundLayer(float f, int i, int j) {
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

        Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation("growableresources", "textures/gui/guiShredder.png"));
        drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);

        int progressWidth = (int) (((float) shredder.shredder.currentProgress / 200F) * 17F);
        int fuelHeight = (int) (((float) shredder.shredder.currentFuelLevel / (float) shredder.shredder.maxFuelLevel) * 13F);
        if (fuelHeight > 13) fuelHeight = 13;

        if (progressWidth > 0) {
            drawTexturedModalRect(guiLeft + 85, guiTop + 40, xSize, 14, progressWidth, 15);
        }
        if (fuelHeight > 0) {
            drawTexturedModalRect(guiLeft + 57, guiTop + 53 - fuelHeight, xSize, 13 - fuelHeight, 14, fuelHeight);
        }
    }

    @Override
    public void drawGuiContainerForegroundLayer(int x, int y) {
        fontRendererObj.drawString("Reed Shredder", 50, 6, 4210752);
    }
}

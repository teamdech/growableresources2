package teamdech.growableresources.gui;

import cpw.mods.fml.common.network.IGuiHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class GUIHandler implements IGuiHandler {
    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        switch (ID) {
            case 0:
                return new ContainerShredder(player.inventory, world.getTileEntity(x, y, z));
            case 1:
                return new ContainerInfuser(player.inventory, world.getTileEntity(x, y, z));
            case 2:
                return new ContainerGuide();
            case 3:
                return new ContainerInfoTool(x, y, z, world);
        }
        return null;
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        switch (ID) {
            case 0:
                return new UIShredder((ContainerShredder) getServerGuiElement(ID, player, world, x, y, z));
            case 1:
                return new UIInfuser((ContainerInfuser) getServerGuiElement(ID, player, world, x, y, z));
            case 2:
                return new UIGuide((ContainerGuide) getServerGuiElement(ID, player, world, x, y, z));
            case 3:
                return new UIInfoTool((ContainerInfoTool) getServerGuiElement(ID, player, world, x, y, z));
        }
        return null;
    }
}

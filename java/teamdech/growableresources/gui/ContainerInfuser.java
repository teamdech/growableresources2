package teamdech.growableresources.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import teamdech.growableresources.blocks.TileEntityInfuser;

public class ContainerInfuser extends Container {
    public TileEntityInfuser infuser;

    public boolean lastUpdatedLocked = false;
    public int lastUpdatedProgress = 0;

    public ContainerInfuser(InventoryPlayer invPlayer, TileEntity tile) {
        infuser = (TileEntityInfuser) tile;

        for (int x = 0; x < 9; x++) {
            addSlotToContainer(new Slot(invPlayer, x, 8 + 18 * x, 142));
        }
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 9; x++) {
                addSlotToContainer(new Slot(invPlayer, x + y * 9 + 9, 8 + 18 * x, 84 + y * 18));
            }
        }

        addSlotToContainer(new SlotInfuser(infuser, 0, 40, 10, "MATERIAL"));
        addSlotToContainer(new SlotInfuser(infuser, 1, 62, 13, "MATERIAL"));
        addSlotToContainer(new SlotInfuser(infuser, 2, 65, 35, "MATERIAL"));
        addSlotToContainer(new SlotInfuser(infuser, 3, 62, 57, "MATERIAL"));
        addSlotToContainer(new SlotInfuser(infuser, 4, 40, 60, "MATERIAL"));
        addSlotToContainer(new SlotInfuser(infuser, 5, 18, 57, "MATERIAL"));
        addSlotToContainer(new SlotInfuser(infuser, 6, 15, 35, "MATERIAL"));
        addSlotToContainer(new SlotInfuser(infuser, 7, 18, 13, "MATERIAL"));

        addSlotToContainer(new SlotInfuser(infuser, 8, 40, 35, "INPUT"));
        addSlotToContainer(new SlotInfuser(infuser, 9, 91, 58, "FUEL"));
        addSlotToContainer(new SlotInfuser(infuser, 10, 124, 35, "OUTPUT"));
    }

    @Override
    public void addCraftingToCrafters(ICrafting player) {
        super.addCraftingToCrafters(player);

        player.sendProgressBarUpdate(this, 0, infuser.locked ? 1 : 0);
        player.sendProgressBarUpdate(this, 1, infuser.progress);
    }

    @Override
    public void updateProgressBar(int channel, int data) {
        if (channel == 0) infuser.locked = data == 1;
        if (channel == 1) infuser.progress = data;
    }

    @Override
    public void detectAndSendChanges() {
        super.detectAndSendChanges();

        if (infuser.locked != lastUpdatedLocked) {
            for (Object player : crafters) ((ICrafting) player).sendProgressBarUpdate(this, 0, infuser.locked ? 1 : 0);
            lastUpdatedLocked = infuser.locked;
        }
        if (infuser.progress != lastUpdatedProgress) {
            for (Object player : crafters) ((ICrafting) player).sendProgressBarUpdate(this, 1, infuser.progress);
            lastUpdatedProgress = infuser.progress;
        }
    }

    @Override
    public boolean canInteractWith(EntityPlayer entityplayer) {
        return infuser.isUseableByPlayer(entityplayer);
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slotID) {
        return null;
    }
}

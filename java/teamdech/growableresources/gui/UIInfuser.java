package teamdech.growableresources.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class UIInfuser extends GuiContainer {
    public ContainerInfuser infuser;

    public UIInfuser(ContainerInfuser infuser) {
        super(infuser);

        this.infuser = infuser;

        xSize = 176;
        ySize = 166;
    }

    @Override
    public void drawGuiContainerBackgroundLayer(float f, int i, int j) {
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

        Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation("growableresources", "textures/gui/guiInfuser.png"));
        drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);

        int progressWidth = (int) (((float) infuser.infuser.progress / 1200F) * 18F);

        if (progressWidth > 0) {
            drawTexturedModalRect(guiLeft + 90, guiTop + 37, xSize, 0, progressWidth, 11);
        }
    }

    @Override
    public void drawGuiContainerForegroundLayer(int x, int y) {
        fontRendererObj.drawString("Infuser", 90, 6, 4210752);
    }
}

package teamdech.growableresources.misc;

import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class CreativeTabGR extends CreativeTabs {
    public CreativeTabGR(String label) {
        super(label);
        LanguageRegistry.instance().addStringLocalization("itemGroup.growableresources", "Growable Resources");
    }

    @Override
    public ItemStack getIconItemStack() {
        return new ItemStack(Items.reeds);
    }

    @Override
    public Item getTabIconItem() {
        return Items.reeds;
    }
}

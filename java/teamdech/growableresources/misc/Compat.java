package teamdech.growableresources.misc;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import teamdech.growableresources.recipe.Recipes;
import teamdech.growableresources.resources.Resource;
import teamdech.growableresources.resources.Resources;
import teamdech.growableresources.util.GRUtil;

import java.util.ArrayList;

public class Compat {
    static String[] compatibleItems = {"ingotCopper", "ingotTin", "itemGlowCrystalWhite", "itemGlowCrystalBlack", "itemGlowCrystalRed", "itemGlowCrystalOrange", "itemGlowCrystalYellow", "itemGlowCrystalGreen", "itemGlowCrystalLightBlue", "itemGlowCrystalBlue", "itemGlowCrystalPurple", "itemGlowCrystalPink", "itemGlowCrystalSpecial", "ingotShadow"};
    static int delta;

    public static void doYourThing() {
        delta = 35;

        addExtraOreDictNames();

        for (int i = 0; i < compatibleItems.length; i++) {
            Resource resource = Resources.getResourceByID(delta + i);
            if (GRUtil.hasOreDictEntries(compatibleItems[i])) {
                resource.product = GRUtil.getFirstStack(compatibleItems[i]);
            } else {
                resource.disable();
            }
        }

        addOreDict(delta, 5, "ingotCopper");
        addOreDict(delta, 50, "blockCopper");
        addOreDict(delta + 1, 5, "ingotTin");
        addOreDict(delta + 1, 50, "blockTin");
        addOreDict(delta + 2, 8, "itemGlowCrystalWhite");
        addOreDict(delta + 3, 8, "itemGlowCrystalBlack");
        addOreDict(delta + 4, 8, "itemGlowCrystalRed");
        addOreDict(delta + 5, 8, "itemGlowCrystalOrange");
        addOreDict(delta + 6, 8, "itemGlowCrystalYellow");
        addOreDict(delta + 7, 8, "itemGlowCrystalGreen");
        addOreDict(delta + 8, 8, "itemGlowCrystalLightBlue");
        addOreDict(delta + 9, 8, "itemGlowCrystalBlue");
        addOreDict(delta + 10, 8, "itemGlowCrystalPurple");
        addOreDict(delta + 11, 8, "itemGlowCrystalPink");
        addOreDict(delta + 12, 8, "itemGlowCrystalSpecial");
        addOreDict(delta + 13, 8, "ingotShadow");
    }

    private static void addExtraOreDictNames() {
        try {
            int cnt_ = 0;
            Item temp = (Item) Class.forName("kkaylium.GlowGlass.items.GGItems").getField("glowCrystal").get(null);
            for (String s : compatibleItems) {
                if (s.contains("itemGlowCrystal")) {
                    OreDictionary.registerOre(s, new ItemStack(temp, 1, cnt_));
                    cnt_++;
                }
            }
            System.out.println("[GR] Enabled GlowGlass integration!");
        } catch (Exception e) {
        }
    }

    private static void addOreDict(int id, int p, String name) {
        if (GRUtil.hasOreDictEntries(name)) {
            ArrayList<ItemStack> items = OreDictionary.getOres(name);
            for (int i = 0; i < items.size(); i++)
                Recipes.infusionCrafting.add(id, p, items.get(i).copy());
        }
    }
}

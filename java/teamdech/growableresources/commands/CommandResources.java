package teamdech.growableresources.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import teamdech.growableresources.items.Items;
import teamdech.growableresources.resources.Resource;
import teamdech.growableresources.resources.Resources;
import teamdech.growableresources.util.GRConfig;
import teamdech.growableresources.util.GRUtil;

import java.util.List;

/**
 * User: jmesserli / Date: 14.12.13 / Time: 09:04
 */
public class CommandResources extends CommandBase {

    @Override
    public int compareTo(Object o) {
        return 0;
    }

    @Override
    public List addTabCompletionOptions(ICommandSender commandSender, String[] args) {
        switch (args.length) {
            case 1:
                return getListOfStringsMatchingLastWord(args, new String[]{"info", "cheat", "list"});
            case 2:
                return getListOfStringsMatchingLastWord(args, Resources.getCompleteList());
        }
        return null;
    }

    @Override
    public int getRequiredPermissionLevel() {
        return GRConfig.enableOverpoweredFeatures ? 4 : 2;
    }

    @Override
    public String getCommandName() {
        return "gresources";
    }

    @Override
    public String getCommandUsage(ICommandSender icommandsender) {
        return "/gresources <list | info <reedID | part of name> | cheat <reedID | part of name> [ammount]";
    }

    @Override
    public void processCommand(ICommandSender icommandsender, String[] astring) {
        switch (astring.length) {
            case 0:
                sendCommandUsageToPlayer(icommandsender);
                break;
            case 1: // list or info, cheat without parameters
                if (!astring[0].equals("list")) sendCommandUsageToPlayer(icommandsender);
                else {
                    StringBuffer message = new StringBuffer();
                    for (int i = 0; i < Resources.getNextAvailableID(); i++) {
                        Resource resource = Resources.getResourceByID(i);
                        message.append(String.format("%s%d: %s%s%s", EnumChatFormatting.DARK_AQUA, resource.id, EnumChatFormatting.DARK_GREEN, resource.name, i == Resources.getNextAvailableID() - 1 ? "" : ", "));
                    }
                    icommandsender.addChatMessage(new ChatComponentText(message.toString()));
                }

                break;
            case 2: // info <ID | part of name> or grcheat <reedID | part of name>
                if (!(astring[0].equals("info") || astring[0].equals("cheat")))
                    sendCommandUsageToPlayer(icommandsender);
                else {
                    Resource resource;
                    try {
                        resource = getResource(astring[1]);
                    } catch (IllegalStateException e) {
                        icommandsender.addChatMessage(new ChatComponentText(EnumChatFormatting.DARK_RED + "Reed '" + astring[1] + "' does not exist! Try /gresources list"));
                        break;
                    }

                    if (astring[0].equals("info")) {
                        icommandsender.addChatMessage(new ChatComponentText(String.format("%s%d: %s%s", EnumChatFormatting.DARK_AQUA, resource.id, EnumChatFormatting.DARK_GREEN, resource.name)));
                    } else {
                        if (!(icommandsender instanceof EntityPlayer)) {
                            icommandsender.addChatMessage(new ChatComponentText("Only players can run that command!"));
                            break;
                        }
                        EntityPlayer player = (EntityPlayer) icommandsender;
                        GRUtil.spawnItemInWorld(icommandsender.getEntityWorld(), new ItemStack(Items.itemReedRoots, 64, resource.id), player.posX, player.posY, player.posZ, 5, 0);
                    }
                }
                break;
            case 3:
                if (!astring[0].equals("cheat")) sendCommandUsageToPlayer(icommandsender);
                else {
                    Resource resource;
                    try {
                        resource = getResource(astring[1]);
                    } catch (IllegalStateException e) {
                        icommandsender.addChatMessage(new ChatComponentText(EnumChatFormatting.DARK_RED + "Reed '" + astring[1] + "' does not exist! Try /gresources list"));
                        break;
                    }
                    if (!(icommandsender instanceof EntityPlayer)) {
                        icommandsender.addChatMessage(new ChatComponentText("Only players can run that command!"));
                        break;
                    }
                    EntityPlayer player = (EntityPlayer) icommandsender;
                    try {
                        GRUtil.spawnItemInWorld(icommandsender.getEntityWorld(), new ItemStack(Items.itemReedRoots, Integer.parseInt(astring[2]), resource.id), player.posX, player.posY, player.posZ, 5, 0);
                    } catch (NumberFormatException e) {
                        sendCommandUsageToPlayer(icommandsender);
                    }
                }
                break;
        }
    }

    private void sendCommandUsageToPlayer(ICommandSender iCommandSender) {
        iCommandSender.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + getCommandUsage(iCommandSender)));
    }

    private Resource getResource(String resString) {
        int i = 0;
        try {
            i = Integer.parseInt(resString);
            return Resources.getResourceByID(i);
        } catch (NumberFormatException e) {
            return Resources.getResourceByName(resString);
        }
    }
}

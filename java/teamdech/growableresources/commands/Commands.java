package teamdech.growableresources.commands;

import cpw.mods.fml.common.event.FMLServerStartingEvent;

/**
 * User: joel / Date: 14.12.13 / Time: 09:02
 */
public class Commands {

    public static void registerCommands(FMLServerStartingEvent event) {
        event.registerServerCommand(new CommandResources());
    }
}
